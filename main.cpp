#include <algorithm>
#include <chrono>
#include <cstdlib>
#include <exception>
#include <iostream>
#include <list>
#include <memory.h>
#include <raylib.h>
#include <stdexcept>
#include <string>
#ifndef TICK_ON_KEYPRESS
#include <queue>
#endif
// #define TICK_ON_KEYPRESS
typedef long time_t;
#define CELL_SIZE 20
#define SCREEN_SIZE 10
static_assert(SCREEN_SIZE > 0, "SCREEN_SIZE must be greater than 0");
static_assert(CELL_SIZE > 0, "CELL_SIZE must be greater than 0");
enum Direction { UP, DOWN, RIGHT, LEFT, NONE };
enum State { GAME_OVER, RUNNING, WIN };
struct pos_t {
  int x;
  int y;
};
struct snake_tile {
  pos_t position;
  Direction dir;
};
struct game {
  std::list<snake_tile> snake;
  unsigned int size = 2;
  pos_t fruit;
  State state = RUNNING;
  #ifndef TICK_ON_KEYPRESS
  std::queue<int> keys;
  #endif
};
time_t timeSinceEpochMillisec() {
  using namespace std::chrono;
  return duration_cast<milliseconds>(system_clock::now().time_since_epoch())
      .count();
}

int rand(int limit) { return rand() % limit; }
game globalGame;
void initGame() {
  srand(timeSinceEpochMillisec());
  globalGame = game();
  globalGame.state = RUNNING;
  snake_tile head = {.position = {rand(SCREEN_SIZE), rand(SCREEN_SIZE)},
                     .dir = (Direction)(rand() % 4)};
  globalGame.snake.push_back(head);
  globalGame.fruit = {rand(SCREEN_SIZE), rand(SCREEN_SIZE)};
  
}
void win() { globalGame.state = WIN; }
void gameOver() { globalGame.state = GAME_OVER; }
void tick(time_t time);
void handleKeypress(int key) {
  if (key == KEY_R && globalGame.state != RUNNING) {
    initGame();
  }
  if (globalGame.state != RUNNING) {
    return;
  }
  auto lastDir = globalGame.snake.back().dir;
  auto shouldReturn = false;

  switch (key) {
  case KEY_UP:
    if (lastDir != DOWN) {
      globalGame.snake.back().dir = UP;
      shouldReturn = true;
    }
    break;
  case KEY_DOWN:
    if (lastDir != UP) {
      globalGame.snake.back().dir = DOWN;
      shouldReturn = true;
    }
    break;
  case KEY_RIGHT:
    if (lastDir != LEFT) {
      globalGame.snake.back().dir = RIGHT;

      shouldReturn = true;
    }
    break;
  case KEY_LEFT:
    if (lastDir != RIGHT) {
      globalGame.snake.back().dir = LEFT;
      shouldReturn = true;
    }
    break;
  }
#ifdef TICK_ON_KEYPRESS
  if (shouldReturn && (key == KEY_UP || key == KEY_DOWN || key == KEY_RIGHT ||
                       key == KEY_LEFT)) {
    tick(timeSinceEpochMillisec());
  }
#endif
}
time_t lastTime;
void tick(time_t time) {
  auto dir = globalGame.snake.back().dir;
  int x = 0;
  int y = 0;
  #ifndef TICK_ON_KEYPRESS
  if(!globalGame.keys.empty()){
    handleKeypress(globalGame.keys.front());
    globalGame.keys.pop();
    dir = globalGame.snake.back().dir;
  }
  #endif
  switch (dir) {
  case UP:
    y = -1;
    break;
  case DOWN:
    y = 1;
    break;
  case RIGHT:
    x = 1;
    break;
  case LEFT:
    x = -1;
    break;
  default:
    throw std::runtime_error("The dir on the head is invalid!");
  }
  snake_tile tile = snake_tile(globalGame.snake.back());
  tile.position.x += x;
  tile.position.y += y;
  if (tile.position.x >= SCREEN_SIZE) {
    tile.position.x = 0;
  }
  if (tile.position.y >= SCREEN_SIZE) {
    tile.position.y = 0;
  }
  if (tile.position.x < 0) {
    tile.position.x = SCREEN_SIZE - 1;
  }
  if (tile.position.y < 0) {
    tile.position.y = SCREEN_SIZE - 1;
  }

  globalGame.snake.push_back(tile);
  if (globalGame.snake.size() == globalGame.size + 1) {
    globalGame.snake.pop_front();
  }
  if (tile.position.x == globalGame.fruit.x &&
      tile.position.y == globalGame.fruit.y) {
    globalGame.size++;
    globalGame.fruit = {rand(SCREEN_SIZE), rand(SCREEN_SIZE)};
    if (globalGame.size == SCREEN_SIZE * SCREEN_SIZE) {
      win();
    }
  }

  for (auto &tailTile : globalGame.snake) {
    if (&tailTile == &globalGame.snake.back() ||
        (tailTile.position.x == tile.position.x - x &&
         tailTile.position.y == tile.position.y - y)) {
      continue;
    }
    if (tailTile.position.x == tile.position.x &&
        tailTile.position.y == tile.position.y) {
      gameOver();
    }
  }
  lastTime = time;
}
void drawSnake() {
  int i = 0;
  float j = 1.0f / globalGame.snake.size();
  for (auto &tile : globalGame.snake) {
    DrawRectangle(tile.position.x * CELL_SIZE, tile.position.y * CELL_SIZE,
                  CELL_SIZE, CELL_SIZE,
                  {
                      .r = 0,
                      .g = (unsigned char)(j * i * 255),
                      .b = (unsigned char)((1 - (j * i)) * 255),
                      .a = 255,
                  });
    i++;
  }
}
void drawFruit() {
  DrawRectangle(globalGame.fruit.x * CELL_SIZE, globalGame.fruit.y * CELL_SIZE,
                CELL_SIZE, CELL_SIZE, RED);
}
void drawBackground() {
  DrawRectangle(0, 0, SCREEN_SIZE * CELL_SIZE, SCREEN_SIZE * CELL_SIZE,
                LIGHTGRAY);
}
int main() {
  initGame();
  InitWindow(SCREEN_SIZE * CELL_SIZE, SCREEN_SIZE * CELL_SIZE, "2048");
  SetWindowState(FLAG_WINDOW_RESIZABLE);
  int key = 0;
  #if PLATFORM == Web
  tick(timeSinceEpochMillisec());
  #endif
  while (!WindowShouldClose()) {
    BeginDrawing();
    while ((key = GetKeyPressed())) {
      #ifdef TICK_ON_KEYPRESS
      handleKeypress(key);
      #else
      globalGame.keys.push(key);
      #endif
    }
    time_t currentTime = timeSinceEpochMillisec();
    if (currentTime - lastTime > 500 && globalGame.state == RUNNING) {
      tick(currentTime);
    }
    drawBackground();
    drawSnake();
    drawFruit();
    auto winText = "Win!";
    auto gameOverText = "Game Over";
    switch (globalGame.state) {

    case WIN:

      DrawText(winText,
               ((SCREEN_SIZE * CELL_SIZE) / 2) - (MeasureText(winText, 20) / 2),
               ((SCREEN_SIZE * CELL_SIZE) / 2) - (MeasureText(winText, 20) / 2),
               20, BLACK);
      break;
    case GAME_OVER:
      DrawText(
          gameOverText,
          ((SCREEN_SIZE * CELL_SIZE) / 2) - (MeasureText(gameOverText, 20) / 2),
          ((SCREEN_SIZE * CELL_SIZE) / 2) - (MeasureText(gameOverText, 20) / 2),
          20, BLACK);
      break;
    default:
      break;
    }
    ClearBackground(RAYWHITE);
    EndDrawing();
  }
  CloseWindow();
  return 0;
}
